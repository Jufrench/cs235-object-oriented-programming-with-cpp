#ifndef _CRITTER
#define _CRITTER

#include <string>
using namespace std;

class CritterManager; // Foreward declaration

class Critter
{
    public:
    Critter();
    void Draw();
    void Update();
    void SetGoal( int x, int y );
    bool HasGoal();
    void CaughtWorm();
    void OutputLog();

    private:
    string m_name;
    char m_symbol;
    int m_x;
    int m_y;
    int m_goalX;
    int m_goalY;
    bool m_alive;
    string m_log;
    int m_eaten;

    friend class CritterManager; // Friends!
};

#endif
