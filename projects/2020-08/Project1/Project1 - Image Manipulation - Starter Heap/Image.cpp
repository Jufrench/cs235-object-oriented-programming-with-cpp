#include "Image.hpp"

#include <fstream>
#include <iostream>
#include <exception>
using namespace std;

PpmImage::PpmImage()
{
    m_pixels = new Pixel*[IMAGE_WIDTH];

    for ( int i = 0; i < IMAGE_WIDTH; i++ )
    {
        m_pixels[i] = new Pixel[IMAGE_HEIGHT];
    }
}

PpmImage::~PpmImage()
{
    for ( int i = 0; i < IMAGE_WIDTH; i++ )
    {
        delete [] m_pixels[i];
    }

    delete [] m_pixels;
}

void PpmImage::LoadImage( const string& filename )
{
    cout << "Loading image from \"" << filename << "\"...";

    throw runtime_error( "Method not implemented!" );

    cout << "SUCCESS" << endl;
}

void PpmImage::SaveImage( const string& filename )
{
    cout << "Saving image to \"" << filename << "\"...";

    throw runtime_error( "Method not implemented!" );

    cout << "SUCCESS" << endl;
}

void PpmImage::ApplyFilter1()
{
    cout << "Applying filter 1...";

    throw runtime_error( "Method not implemented!" );

    cout << "SUCCESS" << endl;
}

void PpmImage::ApplyFilter2()
{
    cout << "Applying filter 2...";

    throw runtime_error( "Method not implemented!" );

    cout << "SUCCESS" << endl;
}
