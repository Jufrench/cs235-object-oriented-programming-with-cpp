#include "Kitten.hpp"

#include <iostream>
#include <iomanip>
using namespace std;

int Kitten::s_counter = 0;

Kitten::Kitten()
{
    s_counter++;
    cout << "s_counter is now " << s_counter << endl;
}

Kitten::~Kitten()
{
    s_counter--;
    cout << "s_counter is now " << s_counter << endl;
}

int Kitten::GetCount()
{
    return s_counter;
}

void Kitten::Setup( string name, int age )
{
    m_age = age;
    m_name = name;
}

void Kitten::Display()
{
    cout << left << setw( 10 )
        << m_name << setw( 10 )
        << m_age << endl;
}
