#ifndef _TEXT_ADVENTURE_HPP
#define _TEXT_ADVENTURE_HPP

#include "Room.hpp"

class TextAdventure
{
    public:
    TextAdventure();
    ~TextAdventure();
    void Run();

    private:
    bool m_isDone;

    string GetUserCommand();
};

#endif
