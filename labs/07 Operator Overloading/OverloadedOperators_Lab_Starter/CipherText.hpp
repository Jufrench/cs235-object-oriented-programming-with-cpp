#ifndef _CIPHER_TEXT
#define _CIPHER_TEXT

#include <iostream>
#include <string>
using namespace std;

class CipherText
{
    public:
    CipherText();
    CipherText( const CipherText& other );
    CipherText( const CipherText* other );
    CipherText& operator=( const CipherText& other );

    void SetText( string text );
    string GetText() const;
    int GetOffset() const;
    void Display() const;

    friend CipherText operator+( const CipherText& item, int amount );
    friend CipherText operator-( const CipherText& item, int amount );

    friend bool operator==( const CipherText& a, const CipherText& b );
    friend ostream& operator<<( ostream& out, CipherText& item );
    friend istream& operator>>( istream& in, CipherText& item );
    CipherText operator[] ( const int index );

    private:
    int m_offset;
    string m_text;

    void OffsetText( int amount );
    CipherText RemoveOffset();
};

#endif
